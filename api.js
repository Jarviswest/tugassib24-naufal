let urlApi = "https://crudcrud.com/api/90778f4811004d7183172717230d0e53/unicorns";

function getData() {
    fetch(urlApi)
    .then(response => response.json())
    .then(data => {
        // Clear existing data
        document.getElementById('product-list').innerHTML = '';
        
        // Loop through the data and display each item
        data.forEach(item => {
            let listItem = document.createElement('li');
            listItem.textContent = `Nama: ${item.nama}, Harga: ${item.price}, Deskripsi: ${item.description}`;
            document.getElementById('product-list').appendChild(listItem);
        });
    })
    .catch(error => {
        console.error('Error:', error);
    });
}

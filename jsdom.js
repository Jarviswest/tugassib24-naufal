document.addEventListener('DOMContentLoaded', function () {
    const prevBtn = document.querySelector('.prev-btn');
    const nextBtn = document.querySelector('.next-btn');
    const slideImages = document.querySelectorAll('.carousel-slide img');
    let currentIndex = 0;
  
    // Fungsi untuk menampilkan gambar sesuai dengan index yang diberikan
    function showImage(index) {
      slideImages.forEach((img, idx) => {
        if (idx === index) {
          img.style.display = 'block';
        } else {
          img.style.display = 'none';
        }
      });
    }
  
    // Menampilkan gambar pertama ketika halaman dimuat
    showImage(currentIndex);
  
    // Event listener untuk tombol "Prev"
    prevBtn.addEventListener('click', function () {
      currentIndex = (currentIndex === 0) ? slideImages.length - 1 : currentIndex - 1;
      showImage(currentIndex);
    });
  
    // Event listener untuk tombol "Next"
    nextBtn.addEventListener('click', function () {
      currentIndex = (currentIndex === slideImages.length - 1) ? 0 : currentIndex + 1;
      showImage(currentIndex);
    });
  });
  